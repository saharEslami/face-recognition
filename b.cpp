LL count_indepen() {
    int val = 0;
    for(int s=0;s<1<<szL;s++) {
        bool ok = 1;
        for(int i=0;i<szL;i++){
            if((s>>i&1)) {
                for(int j=0;j<szL;j++){
                    if(s>>j&1) {
                        if(mp[i][j]) ok = 0;
                    }
                }
            }
        }
        valid[s] = L[s] = ok;
        if(ok) val ++;
    }
    //printf("# %d\n", val);
    for (int bit = 0; bit < szL; bit ++) {
        for (int i = 1; i < (1<<szL); i ++) {
            if ( ((i>>bit) & 1) == 1 )
                L[i] += L[i^(1<<bit)];
        }
    }
 
    for(int s=0;s<1<<szR;s++) {
        bool ok = 1;
        for(int i=0;i<szR;i++){
            if((s>>i&1)) {
                for(int j=0;j<szR;j++){
                    if(s>>j&1) {
                        if(mp[i+szL][j+szL]) ok = 0;
                    }
                }
            }
        }
        //if (ok) printf("rig mask = %d\n", s);
        R[s] = ok;
    }
    for (int bit = 0; bit < szR; bit ++) {
        for (int i = 1; i < (1<<szR); i ++) {
            if ( ((i>>bit) & 1) == 1 )
                R[i] += R[i^(1<<bit)];
        }
    }
 
    for(int i=0;i<szL;i++) {
        for(int j=0;j<n;j++) {
            if(j>=szL && mp[i][j]) {
                nex[i] |= (1<<(j-szL));
            }
        }
    }
    for(int s=1;s<1<<szL;s++) {
        for(int i=0;i<szL;i++) {
            if(s>>i&1) {
                banset[s] = banset[s ^ (1<<i)] | nex[i];
            }
        }
        //printf("banset[%d] = %d\n", s, banset[s]);
    }
 
    LL res = 0;
    for(int s=0;s<1<<szL;s++) {
        if(valid[s]) {
            //printf("s = %d, ok = %d, cnt = %d\n", s, ((1<<szR) - 1) ^ banset[s], R[((1<<szR) - 1) ^ banset[s]]);
            int rigset = ((1<<szR) - 1) ^ banset[s];
            res = res + R[rigset];
        }
    }
    //res --; // substract no red point
    //printf("res = %lld\n", res);
    return res * 2;
}